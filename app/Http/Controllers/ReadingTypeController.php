<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Resources\ReadingTypeResource;
use App\Http\Repositories\ReadingTypeRepository;
use App\Http\Requests\PollutantRequest;
use App\Http\Requests\PollutantUpdateRequest;
use App\ReadingType;

class ReadingTypeController extends Controller
{
    private $repository;

    public function __construct(ReadingTypeRepository $readingTypeRepository) {
        $this->repository = $readingTypeRepository;
    }

    public function getPollutants() {
        $pollutants = $this->repository->findByField('data_type', 'pollutant');
        return ReadingTypeResource::collection($pollutants);
    }

    public function storePollutant(PollutantRequest $request){

        $pollutantData = [
            'type' => $request['type'],
            'description' => $request['description'],
            'data_type' => 'pollutant'
        ];

        $pollutant = $this->repository->create($pollutantData);

        return response()->json($pollutant, 201);
    }

    public function updatePollutant(PollutantUpdateRequest $request, $pollutant){

        $pollutantData = [
            'description' => $request['description']
        ];

        $pollutant = $this->repository->update($pollutantData, $pollutant);

        return response()->json($pollutant, 200);
    }

    public function showPollutant($pollutant_id)
    {
        $pollutant = $this->repository->findByField('id', $pollutant_id)->first();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}

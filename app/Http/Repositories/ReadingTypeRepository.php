<?php

namespace App\Http\Repositories;

use Prettus\Repository\Eloquent\BaseRepository;

class ReadingTypeRepository extends BaseRepository {

    /**
     * Specify Model class name
     *
     * @return string
     */
    function model()
    {
        return "App\\ReadingType";
    }
}

<?php

namespace Tests\Unit;

use App\Http\Controllers\ReadingTypeController;
use App\Http\Repositories\ReadingTypeRepository;
use App\ReadingType;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Mockery;

class ReadingTypeControllerTest extends TestCase
{
    use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */

    const POLLUTANT_NAME = 'PM2.5';
    const POLLUTANT_DESCRIPTION = '';
    const POLLUTANT_DATA_TYPE = 'pollutant';

    private $readingTypeRepositoryMock;
    private $readingTypeController;
    private $testPollutant;
    private $anotherTestPollutant;

    public function setUp() {
        $this->readingTypeRepositoryMock = Mockery::Mock(ReadingTypeRepository::class);
        $this->readingTypeController = new ReadingTypeController($this->readingTypeRepositoryMock);

        $this->testPollutant = new ReadingType([
            'type' => self::POLLUTANT_NAME,
            'description' => self::POLLUTANT_DESCRIPTION,
            'data_type' => self::POLLUTANT_DATA_TYPE
        ]);
        $this->anotherTestPollutant = new ReadingType([
            'type' => self::POLLUTANT_NAME,
            'description' => self::POLLUTANT_DESCRIPTION,
            'data_type' => self::POLLUTANT_DATA_TYPE
        ]);
    }

    public function test__getPollutants_should_call_ReadingTypeRepository_once() {
        $this->readingTypeRepositoryMock->shouldReceive('findByField')->andReturn(collect($this->testPollutant));

        $this->readingTypeController->getPollutants();

        $this->readingTypeRepositoryMock->shouldHaveReceived('findByField')->once();
    }

    public function test__getPollutants_shouldReturnAllThePollutantsAvailable() {
        $this->readingTypeRepositoryMock->shouldReceive('findByField')->andReturn(collect([$this->testPollutant, $this->anotherTestPollutant]));

        $pollutants = $this->readingTypeController->getPollutants();
        $nbOfPollutants = $pollutants->count();

        $EXPECTED_ANSWER = 2;
        $this->assertEquals($EXPECTED_ANSWER, $nbOfPollutants);
    }
}

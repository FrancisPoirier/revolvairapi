<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\User;
use UsersTableSeeder;
use Laravel\Passport\Passport;

class ReadingTypeAPITest extends TestCase
{
  use DatabaseTransactions;
    /**
     * A basic test example.
     *
     * @return void
     */


    public function test_post_pollutant_with_missing_description_returns_unprocessable_entity()
    {
        $response = $this->json('POST', '/api/login',
        ['email' => UsersTableSeeder::ADMIN_EMAIL, 'password' => UsersTableSeeder::PASSWORD]);
        $token = json_decode($response->content())->access_token;

        $data = ['type' => 'testType',];

        $post_response = $this->json('POST', '/api/pollutants', $data,
        ['HTTP_Authorization' => 'Bearer ' . $token]);

        $post_response->assertStatus(422);
    }

    public function test_post_pollutant_with_missing_type_returns_unprocessable_entity()
    {
      $response = $this->json('POST', '/api/login',
          ['email' => UsersTableSeeder::ADMIN_EMAIL, 'password' => UsersTableSeeder::PASSWORD]);
          $token = json_decode($response->content())->access_token;

      $data = ['description' => 'Just a test description'];

      $post_response = $this->json('POST', '/api/pollutants', $data,
    ['HTTP_Authorization' => 'Bearer ' . $token]);

      $post_response->assertStatus(422);
}

    public function test_post_pollutant_with_invalid_description_returns_unprocessable_entity()
    {
      $response = $this->json('POST', '/api/login',
          ['email' => UsersTableSeeder::ADMIN_EMAIL, 'password' => UsersTableSeeder::PASSWORD]);
      $token = json_decode($response->content())->access_token;

      $data = ['type' => 'testType', 'description' => 1];

      $post_response = $this->json('POST', '/api/pollutants', $data,
    ['HTTP_Authorization' => 'Bearer ' . $token]);

      $post_response->assertStatus(422);
    }

    public function test_post_pollutant_with_invalid_type_returns_unprocessable_entity()
    {
       $response = $this->json('POST', '/api/login',
           ['email' => UsersTableSeeder::ADMIN_EMAIL, 'password' => UsersTableSeeder::PASSWORD]);
       $token = json_decode($response->content())->access_token;

       $data = ['type' => 1, 'description' => 'Just a test description'];

       $post_response = $this->json('POST', '/api/pollutants', $data,
     ['HTTP_Authorization' => 'Bearer ' . $token]);

       $post_response->assertStatus(422);
    }

    public function test_post_pollutant_with_admin_scope_returns_success()
    {
        $response = $this->json('POST', '/api/login',
            ['email' => UsersTableSeeder::ADMIN_EMAIL, 'password' => UsersTableSeeder::PASSWORD]);
        $token = json_decode($response->content())->access_token;

        $data = ['type' => 'testType', 'description' => 'Just a test description'];

        $post_response = $this->json('POST', '/api/pollutants', $data,
            ['HTTP_Authorization' => 'Bearer ' . $token]);

        $post_response->assertJson($data);
        $post_response->assertStatus(201);
    }

    public function test_post_pollutant_without_admin_scope_returns_forbiden_access()
    {
        $response = $this->json('POST', '/api/login',
            ['email' => UsersTableSeeder::STATION_OWNER_EMAIL, 'password' => UsersTableSeeder::PASSWORD]);
        $token = json_decode($response->content())->access_token;

        $data = ['type' => 'testType', 'description' => 'Just a test description'];

        $post_response = $this->json('POST', '/api/pollutants', $data,
            ['HTTP_Authorization' => 'Bearer ' . $token]);

        $post_response->assertStatus(403);
    }

    public function test_post_pollutant_without_being_logged_in_returns_unauthorized()
    {
        $data = ['type' => 'testType', 'description' => 'Just a test description'];

        $post_response = $this->json('POST', '/api/pollutants', $data);

        $post_response->assertStatus(401);
    }

    public function test_update_pollutant_with_admin_scope_returns_success()
    {
        $response = $this->json('POST', '/api/login',
            ['email' => UsersTableSeeder::ADMIN_EMAIL, 'password' => UsersTableSeeder::PASSWORD]);
        $token = json_decode($response->content())->access_token;

        $data = ['description' => 'Just a test description'];

        $post_response = $this->json('PUT', '/api/pollutants/2', $data,
      ['HTTP_Authorization' => 'Bearer ' . $token]);

        $post_response->assertJson($data);
        $post_response->assertStatus(200);
    }

    public function test_update_pollutant_without_admin_scope_returns_forbiden_access()
    {
        $response = $this->json('POST', '/api/login',
            ['email' => UsersTableSeeder::STATION_OWNER_EMAIL, 'password' => UsersTableSeeder::PASSWORD]);
        $token = json_decode($response->content())->access_token;

        $data = ['description' => 'Just a test description'];

        $post_response = $this->json('PUT', '/api/pollutants/2', $data,
            ['HTTP_Authorization' => 'Bearer ' . $token]);

        $post_response->assertStatus(403);
    }

    public function test_update_pollutant_without_being_logged_in_returns_unauthorized()
    {
        $data = ['description' => 'Just a test description'];

        $post_response = $this->json('PUT', '/api/pollutants/2', $data);

        $post_response->assertStatus(401);
    }

    public function test_update_pollutant_with_invalid_description_returns_unprocessable_entity()
    {
      $response = $this->json('POST', '/api/login',
          ['email' => UsersTableSeeder::ADMIN_EMAIL, 'password' => UsersTableSeeder::PASSWORD]);
      $token = json_decode($response->content())->access_token;

      $data = ['description' => 1];

      $post_response = $this->json('PUT', '/api/pollutants/2', $data,
            ['HTTP_Authorization' => 'Bearer ' . $token]);

      $post_response->assertStatus(422);
    }
}

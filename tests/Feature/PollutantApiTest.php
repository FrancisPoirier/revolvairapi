<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PollutantApiTest extends TestCase
{
    const FRAGMENT_KEY = 'name';
    const POLLUTANT_PM2P5 = 'PM2.5';
    const POLLUTANT_OZONE = 'Ozone';
    const POLLUTANT_NO2 = 'NO2';
    const POLLUTANT_PM10 = 'PM10';
    const POLLUTANT_CO = 'CO';
    use DatabaseTransactions;
    /**
     * This test depends on the default seeder of pollutants.
     *
     * @return void
     */
    public function test_Get_Pollutants_onTheRightRoute_should_return_all_the_possible_pollutant() {
        //Arrange
        //Act
        $response = $this->json('GET', '/api/pollutants');

        //Assert
        $response->assertJsonFragment([
            self::FRAGMENT_KEY => self::POLLUTANT_PM2P5,
            self::FRAGMENT_KEY => self::POLLUTANT_OZONE,
            self::FRAGMENT_KEY => self::POLLUTANT_NO2,
            self::FRAGMENT_KEY => self::POLLUTANT_PM10,
            self::FRAGMENT_KEY => self::POLLUTANT_CO
        ]);
        $response->assertStatus(200);
    }
}

<?php

use App\ReadingType;
use Illuminate\Database\Seeder;

class ReadingTypesTableSeeder extends Seeder {

    static $TABLE_NAME = 'reading_types';
    static $TYPE_HUMIDITY = 'humidity';
    static $TYPE_PM2P5 = 'PM2.5';
    static $TYPE_PM10 = 'PM10';
    static $TYPE_OZONE = 'Ozone';
    static $TYPE_NO2 = 'NO2';
    static $TYPE_CO = 'CO';
    static $NOT_EXISTING_TYPE = "nacl";
    static $TYPE_TEMPERATURE = 'temperature';
    static $DATA_TYPE_CLIMATIC_DATA = "climatic_data";
    static $DATA_TYPE_POLLUTANT = "pollutant";

    static $TYPE_PM2P5_DESCRIPTION = "<h4>PM 2.5 (fines particules)</h4><br>Particules en suspension inférieur à 2.5 microns ou micromètre. Il s'agit d'un mélange complexe de constituants de taille, de forme, de densité,de surface et de composition chimique différentes.Il suffit d’une faible concentration de particules fines dans l’atmosphère pour nuire à la santé humaine.De nombreuses études ont démontré les effets nuisibles de ces particules respirables, surtout celles ayant un diamètre inférieur à 2,5 microns.</p>";

    static $TYPE_PM10_DESCRIPTION = "<h4>PM 10 (fines particules)</h4><br><p>Particules en suspension inférieur à 10 microns ou micromètre. D'origine naturelle (érosion, volcanisme...) ou anthropique (fumée, usure, etc.),ces particules demeurent plus ou moins longtemps dans l'atmosphère. Les plus grossières (supérieures à 2,5 micromètres) retombent assez vite,tandis que les plus fines peuvent rester plusieurs jours en suspension et parcourir des milliers de kilomètres.</p>";

    static $TYPE_OZONE_DESCRIPTION = "<h4>L'ozone (O3)</h4><br><p>L’ozone est un gaz dont les molécules sont formées de trois atomes d’oxygène (O3).Il est instable et réagit facilement avec d’autres gaz. On le trouve à l’état naturel à plusieurs niveaux de l’atmosphère.À très haute altitude, il nous protège des rayons nocifs du soleil en absorbant une bonne partie du rayonnement ultraviolet.Près du sol, il peut être nuisible pour la santé et l’environnement lorsque les concentrations sont élevées. </p>";

    static $TYPE_NO2_DESCRIPTION = "<h4>Le dioxyde d'azote (NO2)</h4><br><p>Le dioxyde d’azote (NO2) est un gaz irritant généré par tous les processus de combustion. En effet, à haute température,l’azote et l’oxygène de l’air se combinent pour former du monoxyde d’azote (NO). Ce dernier se transforme plus ou moins rapidement en NO2.Ces deux substances, le NO2 et le NO, sont les principales composantes de la famille des oxydes d'azote (NOx). Le NO2 est l'un des constituants du smog;il lui donne sa couleur brunâtre, diminue la visibilité et, lorsqu’il est présent en grande quantité, favorise la formation d’ozone.À la suite de réactions chimiques dans l’atmosphère, le NO2 se transforme en nitrates (sous forme liquide ou solide).</p>";

    static $TYPE_CO_DESCRIPTION = "<h4>Le monoxyde de carbone (CO)</h4><br><p>Le monoxyde de carbone (CO) est un gaz incolore, inodore et insipide produit par la combustion incomplète de toute matière organique,incluant les carburants fossiles (dérivés du pétrole), les déchets et le bois. Une fois dans l'atmosphère, il se transforme éventuellement en dioxyde de carbone (CO2),un des plus importants gaz à effet de serre. En ville, on observe les concentrations maximales de CO aux heures de pointe de circulation automobile,à proximité des autoroutes et des grandes artères urbaines.</p>";

     static $NULL_DESCRIPTION = "";

    public function run() {
        DB::table(ReadingTypesTableSeeder::$TABLE_NAME)->insert([
        [
            'type' => ReadingTypesTableSeeder::$TYPE_HUMIDITY,
            'description' => ReadingTypesTableSeeder::$NULL_DESCRIPTION,
            'data_type' => ReadingTypesTableSeeder::$DATA_TYPE_CLIMATIC_DATA
        ],
        [
            'type' => ReadingTypesTableSeeder::$TYPE_PM2P5,
            'description' => ReadingTypesTableSeeder::$TYPE_PM2P5_DESCRIPTION,
            'data_type' => ReadingTypesTableSeeder::$DATA_TYPE_POLLUTANT

        ],
        [
            'type' => ReadingTypesTableSeeder::$TYPE_PM10,
            'description' => ReadingTypesTableSeeder::$TYPE_PM10_DESCRIPTION,
            'data_type' => ReadingTypesTableSeeder::$DATA_TYPE_POLLUTANT

        ],
        [
            'type' => ReadingTypesTableSeeder::$TYPE_TEMPERATURE,
            'description' => ReadingTypesTableSeeder::$NULL_DESCRIPTION,
            'data_type' => ReadingTypesTableSeeder::$DATA_TYPE_CLIMATIC_DATA
        ],
        [
            'type' => ReadingTypesTableSeeder::$TYPE_OZONE,
            'description' => ReadingTypesTableSeeder::$TYPE_OZONE_DESCRIPTION,
            'data_type' => ReadingTypesTableSeeder::$DATA_TYPE_POLLUTANT
        ],
        [
            'type' => ReadingTypesTableSeeder::$TYPE_NO2,
            'description' => ReadingTypesTableSeeder::$TYPE_NO2_DESCRIPTION,
            'data_type' => ReadingTypesTableSeeder::$DATA_TYPE_POLLUTANT
        ],
        [
            'type' => ReadingTypesTableSeeder::$TYPE_CO,
            'description' => ReadingTypesTableSeeder::$TYPE_CO_DESCRIPTION,
            'data_type' => ReadingTypesTableSeeder::$DATA_TYPE_POLLUTANT
        ],
        ]);
    }
}

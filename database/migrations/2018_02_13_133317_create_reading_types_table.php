<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReadingTypesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reading_types', function (Blueprint $table) {
            $table->increments('id');
            $table->string('type')->unique();
            $table->string('description', 3000)->nullable();
            $table->enum('data_type', ['pollutant', 'climatic_data']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('reading_types');
    }
}
